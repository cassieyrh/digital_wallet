import java.util.HashSet;
import java.util.Set;

/*
 * The user class: each user has a userID and a set containing users he/she has made payments to or receive payments from
 */

public class User {
	private String userID;
	private Set<User> contacts;
	
	public User(String userID) {
		this.userID = userID;
		this.contacts = new HashSet<>();
	}
	
	public Set<User> getContacts() {
		return this.contacts;
	}
	
	public String getUserID() {
		return this.userID;
	}
	
	public void addContact(User u) {
		this.contacts.add(u);
	}
	
	public boolean hasContact(User u) {
		return this.contacts.contains(u);
	}

}
