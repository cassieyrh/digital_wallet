import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

// example of program that detects suspicious transactions
// fraud detection algorithm



public class antifraud{
	/**
	 * 
	 * @param args  Contains Five arguments: 
	 *                  1. File name of data records input: ./paymo_input/batch_payment.txt
	 *                  2. File name of stream data input: ./paymo_input/stream_payment.txt
	 *                  3. File name of output result for Feature1: ./paymo_output/output1.txt
	 *                  4. File name of output result for Feature2: ./paymo_output/output2.txt
	 *                  5. File name of output result for Feature3: ./paymo_output/output3.txt
	 */
	public static void main(String[] args) {
		if (args.length < 5) {
			throw new IllegalArgumentException(
					"The input argument is not valid, please include five file names in the argument");
		}		
		/*
		 * Process data records and build graph
		 */		
		//usermap: contains all users and maps the userID of a user to the corresponding user object
		Map<String, User> usermap = new HashMap<>();		
		try {  
            FileReader fileReader = new FileReader(args[0]);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            //Read the first line while contains titles of columns
            String line = bufferedReader.readLine();
            
            //Read the next lines containing data
            while((line = bufferedReader.readLine()) != null) {
                String[] splited = line.split(",");
                if (splited.length < 3) continue;
                String user1 = splited[1].trim();
                String user2 = splited[2].trim();
                if (!usermap.containsKey(user1)) {
                	usermap.put(user1, new User(user1));
                }
                if (!usermap.containsKey(user2)) {
                	usermap.put(user2, new User(user2));
                }
                usermap.get(user1).addContact(usermap.get(user2));
                usermap.get(user2).addContact(usermap.get(user1));
            }   
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + args[0] + "'");                
        }
        catch(IOException ex) {
            System.out.println("Error reading file '" + args[0] + "'");                  
        }		
		/*
		 * Process stream input data and write output given by Feature1, Feature2, Feature3 to three output files
		 */
		
		//Open three output files to write result
		BufferedWriter[] bufferedWriters = new BufferedWriter[3];
		for (int i = 2; i < 5; i++) {
			try {
	            FileWriter fileWriter = new FileWriter(args[i]);
	            bufferedWriters[i - 2] = new BufferedWriter(fileWriter);
	        }
	        catch(IOException ex) {
	            System.out.println("Error writing to file '"+ args[i] + "'");
	        }
		}
		
		try {  
            FileReader fileReader = new FileReader(args[1]);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            //Read the first line while contains titles of columns
            String line = bufferedReader.readLine();
            
            //Read the next lines containing data
            while((line = bufferedReader.readLine()) != null) {
                String[] splited = line.split(",");
                if (splited.length < 3) continue;
                String user1 = splited[1].trim();
                String user2 = splited[2].trim();
                
                //features: array of 3 boolean variables represents whether the result is trusted or untrusted for the current input
                boolean[] features = new boolean[3];
                //flag: if both users have made payments or received payments before
                //If either one of the user pair have not used the app before, the result should be "unverified".
                boolean flag = true;  
                if (!usermap.containsKey(user1) || !usermap.containsKey(user2)) {
                	flag = false;
                }
                User u1 = usermap.get(user1);
                User u2 = usermap.get(user2);
                if (flag) {
                	features[0] = Search.searchFirstDegree(u1, u2);
                	if (features[0]) {
                		features[1] = true;
                	} else {
                		//only check if they are second degree connection when they are not first degree connection
                		features[1] = Search.searchSecondDegree(u1, u2);
                	}
                	if (features[0] || features[1]) {
                		features[2] = true;
                	} else {
                		//only check if they are four degree connection when they are not first and second degree connection
                		features[2] = Search.searchFourthDegree(u1, u2);
                	}
                }
                for (int i = 0; i < features.length; i++) {
                	if (features[i]) {
                		bufferedWriters[i].write("trusted");
                	} else {
                		bufferedWriters[i].write("unverified");
                	}
                	bufferedWriters[i].newLine();
                }               
            }   
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + args[1] + "'");                
        }
        catch(IOException ex) {
            System.out.println("Error reading file '" + args[1] + "'");                  
        }
		//close bufferedWriters
		for (int i = 0; i < bufferedWriters.length; i++) {
			try {
				bufferedWriters[i].close();
			}
			catch(IOException ex) {
	            System.out.println("Error closing file '"+ args[i] + "'");
	        }
		}
		
	}
}
