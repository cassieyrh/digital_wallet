import java.util.HashSet;
import java.util.Set;

public class Search {
	//return true if two users are directly connected
	public static boolean searchFirstDegree(User user1, User user2) {
		if (user1.hasContact(user2)) {
			return true;
		}
		return false;
	}
	
	//return true if two users are second degree connections
	public static boolean searchSecondDegree(User user1, User user2) {
		//To find if they are two degree connection, find if there are any 
		//intersections between their direct connections
		Set<User> contacts1 = user1.getContacts();
		Set<User> contacts2 = user2.getContacts();
		if (containsIntersection(contacts1, contacts2)) {
			return true;
		}
		return false;
	}
	
	//return true if two users are fourth degree connections
	public static boolean searchFourthDegree(User user1, User user2) {
		Set<User> contacts1 = user1.getContacts();
		Set<User> contacts2 = user2.getContacts();
		Set<User> secondContacts = new HashSet<>();
		int s1 = contacts1.size();
		int s2 = contacts2.size();
		//The idea here is to get the second degree connections of one user who has less direct connections,
		//and then check if his/her second degree connections contains the direct connection of the other user,
		//which means these two users are the third degree connections.
		//Or if his/her second degree connections has intersection with the other person's second degree 
		//connections, they are the fourth degree connections.		
		if (s1 < s2) {
			getSecondContacts(contacts1, secondContacts);
			for (User u: contacts2) {
				if (secondContacts.contains(u) || containsIntersection(u.getContacts(), secondContacts)) {
					return true;
				}
			}
		} else {
			getSecondContacts(contacts2, secondContacts);
			for (User u: contacts1) {
				if (secondContacts.contains(u) || containsIntersection(u.getContacts(), secondContacts)) {
					return true;
				}
			}
		}		
		return false;
	}
	
	//containsIntersect: returns true if there is at least one element contained in both sets
	public static boolean containsIntersection(Set<User> set1, Set<User> set2) {
		int size1 = set1.size();
		int size2 = set2.size();
		if (size1 > size2) {
			for (User u : set2) {
				if (set1.contains(u)) {
					return true;
				}
			}
		} else {
			for (User u : set1) {
				if (set2.contains(u)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static void getSecondContacts(Set<User> set, Set<User> secondContacts) {
		for (User u : set) {
			secondContacts.addAll(u.getContacts());
		}
	}
}
